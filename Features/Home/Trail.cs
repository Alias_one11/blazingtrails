// ReSharper disable All
namespace BlazingTrails.Web.Features.Home 
{
    /** <Trail>:
    ••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    <Home Feature>:
        • Inside this class we need to add a few properties 
        • which represent the various data about a trail, 
        • things like its name, length, and the time it
        • takes to complete it.
    ••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
    */
    public class  Trail {
        ///∆: - ©Global-PROPERTIES
        //∆..............................
        public string Name { get; set; }
        public string Image { get; set; }
        public string Location { get; set; }
        public string Time { get; set; }
        public int Length { get; set; }
        //∆..............................
    }
}